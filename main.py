import wx
import pygame, time
import pygame.camera
from pygame.locals import *
import pylab
from camtrack import *
import pickle

pygame.init()
pygame.camera.init()

c=Capture()

objetos=[Trackable()]
def pinta(objeto):
	x1=pylab.array(objeto.history)
	pylab.subplot(2,2,1)
	pylab.xlabel('X (px)')
	pylab.ylabel('Y (px)')
	pylab.xlim(0,c.size[0])
	pylab.ylim(c.size[1],0)
	pylab.plot(x1[:,0],x1[:,1])
	pylab.subplot(2,2,2)
	pylab.xlabel('t (s)')
	pylab.ylabel('Y (px)')
	pylab.ylim(c.size[1],0)
	pylab.plot(x1[:,2],x1[:,1])
	pylab.subplot(2,2,3)
	pylab.xlabel('X (px)')
	pylab.ylabel('t (s)')
	pylab.xlim(0,c.size[0])
	pylab.plot(x1[:,0],x1[:,2])
	pylab.show()
	
def write_to_file(objetos,directory):
	for i in objetos:
		file=open(directory+'/'+i.name+'_out.dat','w')
		file.write('#  '+i.name+'\n')
		file.write('# ')
		for ii in range((len(i.history[0])-1)/2):
			file.write('X(px)   Y(px)	')
		file.write('t(s) \n')
		for j in i.history:
			for k in j:
				file.write(str(k)+'	')
			file.write('\n')
		file.close()


#size = width, height = 320,240	
screen = pygame.display.set_mode()
pygame.display.iconify()

class Main(wx.Frame):
	def __init__(self, *args, **kwds):
		# begin wxGlade: Main.__init__
		kwds["style"] = wx.DEFAULT_FRAME_STYLE
		wx.Frame.__init__(self, *args, **kwds)
		self.notebook_1 = wx.Notebook(self, -1, style=0)
		self.notebook_1_pane_1 = wx.Panel(self.notebook_1, -1)
		self.bitmap_1 = wx.StaticBitmap(self.notebook_1_pane_1, -1, wx.Bitmap("icon.png", wx.BITMAP_TYPE_ANY))
		self.CaptureButton = wx.Button(self.notebook_1_pane_1, -1, "Capture")
		self.slider_1 = wx.Slider(self.notebook_1_pane_1, -1, 10, 1, 300, style=wx.SL_HORIZONTAL | wx.SL_LABELS)
		self.label_1 = wx.StaticText(self.notebook_1_pane_1, -1, "Select Acquisition time (s)", style=wx.ALIGN_CENTRE)
		self.button_writedata = wx.Button(self.notebook_1_pane_1, -1, "Write data file")
		self.panel_1 = wx.Panel(self.notebook_1, -1)
		self.button_Calibrate = wx.Button(self.panel_1, -1, "Calibrate")
		self.button_New = wx.Button(self.panel_1, -1, "New")
		self.button_Remove = wx.Button(self.panel_1, -1, "Remove")
		self.button_plot = wx.Button(self.panel_1, -1, "Plot Data")
		self.label_2 = wx.StaticText(self.panel_1, -1, "Calibration Procedure:\n- Left Click: Choose Color\n- Mouse Wheel: Adjust Threshold\n- Keyboard: Choose Name\n- Right Click: Finish")
		self.list_box_1 = wx.ListBox(self.panel_1, -1, choices=[])
		self.button_exit = wx.Button(self, -1, "Quit")

		self.__set_properties()
		self.__do_layout()

		self.Bind(wx.EVT_BUTTON, self.on_CaptureButton, self.CaptureButton)
		self.Bind(wx.EVT_BUTTON, self.on_Write, self.button_writedata)
		self.Bind(wx.EVT_BUTTON, self.on_Calibrate, self.button_Calibrate)
		self.Bind(wx.EVT_BUTTON, self.on_New, self.button_New)
		self.Bind(wx.EVT_BUTTON, self.on_Remove, self.button_Remove)
		self.Bind(wx.EVT_BUTTON, self.on_Plot, self.button_plot)
		self.Bind(wx.EVT_BUTTON, self.on_Exit, self.button_exit)
		# end wxGlade
		for o in objetos:
			self.list_box_1.Append(o.name+' - '+str(o.color))

	def __set_properties(self):
		# begin wxGlade: Main.__set_properties
		self.SetTitle("PyCamAcquisitor")
		self.SetSize((417, 329))
		# end wxGlade

	def __do_layout(self):
		# begin wxGlade: Main.__do_layout
		sizer_7 = wx.BoxSizer(wx.VERTICAL)
		sizer_10 = wx.BoxSizer(wx.VERTICAL)
		sizer_11 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_8 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_9 = wx.BoxSizer(wx.VERTICAL)
		sizer_8.Add(self.bitmap_1, 0, 0, 0)
		sizer_9.Add(self.CaptureButton, 0, 0, 0)
		sizer_9.Add(self.slider_1, 0, wx.EXPAND, 0)
		sizer_9.Add(self.label_1, 0, 0, 0)
		sizer_9.Add(self.button_writedata, 0, 0, 0)
		sizer_8.Add(sizer_9, 1, wx.EXPAND, 0)
		self.notebook_1_pane_1.SetSizer(sizer_8)
		sizer_11.Add(self.button_Calibrate, 0, 0, 0)
		sizer_11.Add(self.button_New, 0, 0, 0)
		sizer_11.Add(self.button_Remove, 0, 0, 0)
		sizer_11.Add(self.button_plot, 0, 0, 0)
		sizer_10.Add(sizer_11, 1, wx.SHAPED, 0)
		sizer_10.Add(self.label_2, 0, wx.EXPAND, 0)
		sizer_10.Add(self.list_box_1, 2, wx.EXPAND, 0)
		self.panel_1.SetSizer(sizer_10)
		self.notebook_1.AddPage(self.notebook_1_pane_1, "Capture")
		self.notebook_1.AddPage(self.panel_1, "Objects")
		sizer_7.Add(self.notebook_1, 1, wx.EXPAND, 0)
		sizer_7.Add(self.button_exit, 0, 0, 0)
		self.SetSizer(sizer_7)
		self.Layout()
		# end wxGlade

	def on_CaptureButton(self, event):  # wxGlade: Main.<event_handler>
		#print "Event handler `on_CaptureButton' not implemented!"
		graba(c,objetos,self.slider_1.GetValue())
		event.Skip()

	def on_Calibrate(self, event):  # wxGlade: Main.<event_handler>
		#print "Event handler `on_Calibrate' not implemented!"
		#pygame.display.toggle_fullscreen()
		choose=self.list_box_1.GetSelections()[0]
		acalibrar(c,objetos[choose])
		self.list_box_1.Clear()
		for o in objetos:
			self.list_box_1.Append(o.name+' - '+str(o.color))
		#pygame.display.toggle_fullscreen()
		pygame.display.iconify()
		event.Skip()

	def on_New(self, event):  # wxGlade: Main.<event_handler>
		#print "Event handler `on_New' not implemented!"
		self.list_box_1.Clear()
		objetos.append(Trackable())
		for o in objetos:
			self.list_box_1.Append(o.name+' - '+str(o.color))
		event.Skip()

	def on_Remove(self, event):  # wxGlade: Main.<event_handler>
		#print "Event handler `on_Remove' not implemented!"
		objetos.pop(self.list_box_1.GetSelections()[0])
		self.list_box_1.Clear()
		for o in objetos:
			self.list_box_1.Append(o.name+' - '+str(o.color))
		event.Skip()

	def on_Exit(self, event):  # wxGlade: Main.<event_handler>
		#print "Event handler `on_Exit' not implemented!"
		pygame.quit()
		quit()
		event.Skip()

	def on_Write(self, event):  # wxGlade: Main.<event_handler>
		#print "Event handler `on_Write' not implemented"
		dialog = wx.DirDialog ( None, message = 'Pick a directory.', style = wx.DD_NEW_DIR_BUTTON )
		if dialog.ShowModal() == wx.ID_OK:
			directory=dialog.GetPath()
			print directory
			write_to_file(objetos,directory)
		else:
			print 'No directory.'
		dialog.Destroy()
		event.Skip()

	def on_Plot(self, event):  # wxGlade: Main.<event_handler>
		print "Event handler `on_Plot' not implemented"
		pinta(objetos[self.list_box_1.GetSelections()[0]])
		event.Skip()

# end of class Main
if __name__ == "__main__":
	PyCamAcquisitor = wx.PySimpleApp(0)
	wx.InitAllImageHandlers()
	main_window = Main(None, -1, "")
	PyCamAcquisitor.SetTopWindow(main_window)
	main_window.Show()
	PyCamAcquisitor.MainLoop()
