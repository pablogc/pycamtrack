import pygame, time
import pygame.camera
from pygame.locals import *
import pylab
from scipy import *
from scipy import optimize

class Trackable(object):
    def __init__(self,name='Unnamed'):
        self.color=[1,1,1]
        self.origcolor=[1,1,1]
        self.tolerance=[1,1,1]
        self.pos=[1,1]
        self.vel=[1,1]
        self.size=1
        self.history=[]
        self.howmany=1
        self.name=name

class Model(object):
    def __init__(self):
        self.p0=[100,-0.01,1.,4.,0.]
        self.p1=[0,0,0,0,0]
        self.fitfunc = lambda p, x: p[0]*exp(p[1]*x)*cos(2*pi/p[2]*x+p[3]) + p[4]
        self.errfunc = lambda p, x, y: self.fitfunc(p, x) - y
    def ajusta(self,x,t):
        self.p1, success = optimize.leastsq(self.errfunc, self.p0[:], args=(t, x))
        return success
    def posicion(self,t):
        return self.fitfunc(self.p1,t)
    
    
class Capture(object):
    def __init__(self):
        self.size = (320,240)
        self.display = pygame.display.set_mode(self.size, 0)
        self.clist = pygame.camera.list_cameras()
        self.cam = pygame.camera.Camera(self.clist[0], self.size,'RGB')
        self.cam.start()        
        self.snapshot = pygame.surface.Surface(self.size, 0, self.display)
        self.thresholded = pygame.surface.Surface(self.size, 0, self.display)
        self.time=0
        self.fps=[]
        self.time0=time.time()
        self.pelicula=[]
                         
    def guarda(self,name):
        pygame.image.save(self.snapshot,name)
        
    def get(self):
        while not self.cam.query_image():
            None
        self.fps.append(1/(time.time()-self.time))
        if len(self.fps) > 20:
            self.fps.pop(0)
            #print sum(self.fps)/20
        self.time=time.time()
        self.cam.get_image(self.snapshot)
        self.snapshot = pygame.transform.flip(self.snapshot,True,False)
        self.display.blit(self.snapshot,(0,0))
        
        #self.pelicula.append(self.snapshot)

    
        

    def identify(self,trackable):
        mask = pygame.mask.from_threshold(self.snapshot, trackable.color, trackable.tolerance)
        #self.display.blit(self.snapshot, (0,0))
        connecteds_mask = mask.connected_components()
        connecteds=sorted(connecteds_mask, key=lambda l:l.count(),reverse=True)
        if len(connecteds) >= trackable.howmany :
            coords=[]
            for i in range(trackable.howmany):
                coord = connecteds[i].centroid()
                size = connecteds[i].get_size()
                # Las dos siguientes lineas actualizan el color a seguir en cada paso
                #crect = pygame.draw.rect(self.display, (255, 0, 0), (coord[0]-6,coord[1]-6,12,12), 4)
                #trackable.color=self.snapshot.get_at(coord)
                trackable.pos=coord
                trackable.size=connecteds[i].count()
                pygame.draw.circle(self.display, [255-trackable.color[0],255-trackable.color[1],255-trackable.color[2]], (coord[0],coord[1]), 1+int(pylab.sqrt(trackable.size/pylab.pi)), 1)
                coords.append(coord[0])
                coords.append(coord[1])
                #self.v0[0]=[coord[0],coord[1]]
                p=[]
            coords.append(self.time-self.time0)
            trackable.history.append(coords)
        else:
            coord = (-1,-1)
            coords=[]
            
    def pintabola(self,coord):
        pygame.draw.circle(self.display,[0,255,0],coord,20,0)

    def pintatrayectoria(self,trackable):
        lines=[]
        if len(trackable.history) < 20:
            for n in range(len(trackable.history)):
                lines.append([trackable.history[-1-n][0],trackable.history[-1-n][1]])
            if len(lines) > 2:
                pygame.draw.lines(self.display,(0,255,0),False,lines,1)
        else:
            for n in range(20):
                lines.append([trackable.history[-1-n][0],trackable.history[-1-n][1]])
            pygame.draw.lines(self.display,(0,255,0),False,lines,1)

    def update_image(self):
        #font = pygame.font.Font(None, 20)
        #text = font.render(self.cartelico, 1, (10, 10, 10))
        #textpos = text.get_rect()
        #self.display.blit(text, textpos)
        pygame.display.flip()
        #self.display.blit(self.snapshot,(0,0))
        #return coords

    def proyecta(self):
        dentro=True
        n=0
        while dentro:
            time.sleep(0.05)
            for event in pygame.event.get():
                if ( event.type == pygame.MOUSEBUTTONDOWN and event.button == 4 ):
                    if n > 0 :
                        n-=1
                elif ( event.type == pygame.MOUSEBUTTONDOWN and event.button == 5 ):
                    if n < len(self.pelicula) :
                        n+=1
                elif ( event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 ):
                    dentro=False
            #print n
            self.display.blit(self.pelicula[n],(0,0))
            pygame.display.flip()
                
    def display_text(self,text):    
        font = pygame.font.Font(None, 20)
        text_s = font.render(text, 1, (10, 10, 10))
        textpos = text_s.get_rect()
        self.display.blit(text_s, textpos)
    
        
    def calibrate(self,coords,trackable,n):
        self.snapshot = pygame.transform.flip(self.cam.get_image(self.snapshot),True,False)
        self.display.blit(self.snapshot, (0,0))
        #crect = pygame.draw.rect(self.display, (255, 0, 0), (coords[0]-5,coords[1]-5,10,10), 1)
        #trackable.color = pygame.transform.average_color(self.snapshot, crect)
        trackable.color= self.snapshot.get_at(coords)
        #self.display.fill(trackable.color, (0,0,50,50))
        pygame.display.flip()
        

def acalibrar(c,trackable):
    espera=True
    #trackable.color=[]
    for n in range(trackable.howmany):
        while espera:
            for event in pygame.event.get():
                if ( event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0] ) :
                    c.calibrate(pygame.mouse.get_pos(),trackable,n)
                elif ( event.type == pygame.MOUSEBUTTONDOWN and event.button == 4 ):
                    for i in range(3):
                        if trackable.tolerance[i] < 255:
                            trackable.tolerance[i]+=1
                elif ( event.type == pygame.MOUSEBUTTONDOWN and event.button == 5 ):
                    for i in range(3):
                        if trackable.tolerance[i] > 0:
                            trackable.tolerance[i]-=1
                elif ( event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[2] ):
                    espera=False
                elif event.type == pygame.KEYDOWN:
                    if event.key == K_BACKSPACE:
                        trackable.name=trackable.name[0:-1]
                    elif event.key == K_MINUS:
                        trackable.name+='_'
                    elif event.key <= 127:
                        trackable.name+=chr(event.key)
            c.get()
            c.identify(trackable)
            c.display_text(trackable.name+'='+str(trackable.color)+'+/-'+str(trackable.tolerance))
            c.update_image()
        

    

def graba(c,objetos,tiempo):
    c.time0=time.time()
    for i in objetos:
        i.history=[]
    while time.time()-c.time0 < tiempo:
        c.get()
        for i in objetos:
            c.identify(i)
        #    if len(i.history) == 110:
        #        i.modelo=Model()
        #        flag=i.modelo.ajusta(array([ji[0] for ji in i.history[10:110]]),array([ji[2] for ji in i.history[10:110]]))
        #    elif len(i.history) > 110:
        #        MX=i.modelo.posicion(i.history[-1][2])
        #        c.pintabola((int(MX),i.history[-1][1]))
        c.display_text(str(int(time.time()-c.time0)))
        #time.sleep(0.1)
        c.update_image()
    return 

    
    
    
