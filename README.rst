PyCamTrack
==========

This project provides a really simple tool for real time image based data acquisition intended for teaching physics in laboratories.

With a minimal point and click calibration process, the system will be able to track one or more objects based on their color (gomets or similar stuff could be sticked to objects as markers), and capture time series to be exported as CSV or directly plotted.

Working demos
-------------

 * `Metronome <https://www.youtube.com/watch?v=ep5i2DuGHrY>`_.
 * `Inverted double pendulum <https://www.youtube.com/watch?v=_P0XgkyY71Q>`_.

Minimal requirements
--------------------

 * *Python*
 * pygame 
 * *V4l2* compatible webcam
